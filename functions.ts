/*
* Additional Functions
*/

/*
* Sort object by key
*/
export function SortByKey(object: any): any {
   return Object.keys(object).sort().reduce((objEntries, key) => {
        objEntries[key] = object[key];
        return objEntries;
    }, {});
}

/*
* Get cheapest ingridients
*/
export function GetCheapest(object: any): any {
   return object.reduce((prev: any, curr: any) => prev.cost < curr.cost ? prev : curr);
}