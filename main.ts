import {
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
import {NutrientFact, Product} from "./supporting-files/models";
import {
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits,
} from "./supporting-files/helpers";
import {RunTest, ExpectedRecipeSummary} from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */
import {SumUnitsOfMeasure} from "./supporting-files/helpers";
import {
	SortByKey,
	GetCheapest,
} from "./functions";

recipeData.forEach((recipe) => {
	
	let totalCost: number = 0; /* initialize total cost of recipe */
	let nutrients: any = {}; /* initialize nutrients data */

	recipe.lineItems.forEach((item) => {
		let productsForIngredient: Product[] = GetProductsForIngredient(item.ingredient); /* */
		let productCost: any = []; /* Initialize product cost per supplier w/ nutrients data */

		/* Get product ingridients */
		productsForIngredient.forEach((product) => {

			/* Get product ingridients per supplier */
			product.supplierProducts.forEach(supplier => {
				productCost.push({
					cost: GetCostPerBaseUnit(supplier) * item.unitOfMeasure.uomAmount,	
					nutrientFacts: [...product.nutrientFacts]
				});
			});
			/* End of get product ingridients per supplier */

		});
		/* End of get product ingridients */

		let cheapest: any = GetCheapest(productCost);
		
		/* Calculate nutrients based onn the cheapest ingredients */
		cheapest.nutrientFacts.forEach((nutrientFact: NutrientFact) => {
			let nutrientFactInBaseUnits: NutrientFact = GetNutrientFactInBaseUnits(nutrientFact);
			if (nutrients[nutrientFact.nutrientName]) {
				nutrientFactInBaseUnits.quantityAmount = SumUnitsOfMeasure(
					nutrientFactInBaseUnits.quantityAmount, 
					nutrients[nutrientFact.nutrientName].quantityAmount
				);	
			}
			
			nutrients[nutrientFact.nutrientName] = {
				...nutrientFactInBaseUnits,
			};
		});
		/* End of calculate nutrients based onn the cheapest ingredients */

		totalCost += cheapest.cost;  /* sum of total cost and cheapest ingridients */
	});

	/* Update recipeSummary */
	recipeSummary[recipe.recipeName] = {
		cheapestCost: totalCost,
		nutrientsAtCheapestCost: {...SortByKey(nutrients)}
	};
	/* End of update recipeSummary */
});

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
